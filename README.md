# Info
Testovaci subor sluzby E1 projektu IT4KT.

# Elementy
 - objective (s referenciami aj bez)
 - introduction
 - step (s referenciami aj bez)
 - task (priklad aj uloha, s riesenim aj bez)
 - additional (task, resource)
 - table
 - h:b, h:i, h:u
 - resource
 - comment (samostatne, v ulohe)
 - lecturer (block/inline)
 - code (s deklarovanym jazykom aj bez, inline/block)
 - figure
 - image (vo figure, aj samostatne)
 - caption
 - matematicke zapisy inline/block
 - link
 - quiz (s levelmi, aj bez)
 - presentation